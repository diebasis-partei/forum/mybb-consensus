<?php

class Vote {

    public int $vote_id;
    public int $proposal_id;
    public int $user_id;
    public int $points;

    public function __construct($proposal_id, $user_id, $points, $vote_id = 0) {
        $this->proposal_id = $proposal_id;
        $this->user_id = $user_id;
        $this->points = $points;
        $this->vote_id = $vote_id;
    }

    /**
     * @return int
     */
    public function getVoteId(): int
    {
        return $this->vote_id;
    }

    /**
     * @return int
     */
    public function getProposalId(): int
    {
        return $this->proposal_id;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     */
    public function setPoints(int $points): void
    {
        $this->points = $points;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function toDBArray(): array {
        return array('proposal_id' => $this->proposal_id,
            'user_id' => $this->user_id,
            'points' => $this->points
        );
    }

}
