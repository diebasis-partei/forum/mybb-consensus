<?php

class Proposal
{
    public int $id;
    public string $title;
    public string $description;
    public int $consensus_id;
    public int $position;
    public ?string $filename;

    public function __construct(string $title, string $description, int $position, int $consensus_id, string $filename = null, int $id = 0) {
        $this->id = $id;
        $this->description = $description;
        $this->title = $title;
        $this->position = $position;
        $this->consensus_id = $consensus_id;
        $this->filename = $filename;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getConsensusId(): int
    {
        return $this->consensus_id;
    }

    /**
     * @param int $consensus_id
     */
    public function setConsensusId(int $consensus_id): void
    {
        $this->consensus_id = $consensus_id;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string|null $filename
     */
    public function setFilename(?string $filename): void
    {
        $this->filename = $filename;
    }




    public function toDBArray(): array {
        return array(
            "title" => $this->getTitle(),
            "description" => $this->getDescription(),
            "position" => $this->getPosition(),
            "consensus_id" => $this->getConsensusId(),
            "filename" => $this->getFilename()
        );
    }

}
