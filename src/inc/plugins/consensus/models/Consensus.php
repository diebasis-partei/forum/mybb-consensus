<?php

class Consensus
{
    public int $consensus_id;
    public string $title;
    public string $description;
    public DateTime $expires;
    public int $user_id;
    public int $thread_id;
    public int $status_id;
    public array $proposals;

    public function __construct(string $title,
                                string $description,
                                DateTime $expires,
                                int $user_id,
                                int $thread_id,
                                int $status_id,
                                array $proposals,
                                int $consensus_id = 0)
    {
        $this->title = $title;
        $this->description = $description;
        $this->expires = $expires;
        $this->user_id = $user_id;
        $this->thread_id = $thread_id;
        $this->status_id = $status_id;
        $this->proposals = $proposals;
        $this->consensus_id = $consensus_id;
    }

    public function add_proposal(Proposal $proposal): void {
        $this->proposals[] = $proposal;
    }

    /**
     * @return int
     */
    public function getConsensusId(): int
    {
        return $this->consensus_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return DateTime
     */
    public function getExpires(): DateTime
    {
        return $this->expires;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @return int
     */
    public function getThreadId(): int
    {
        return $this->thread_id;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->status_id;
    }

    /**
     * @return array
     */
    public function getProposals(): array
    {
        return $this->proposals;
    }

    public function toDBArray(): array {
        return array(
                'title' => $this->getTitle(),
                'description' => $this->getDescription(),
                'expires' => $this->getExpires()->format("Y-m-d H:i:s"),
                'creator' => $this->getUserId(),
                'thread_id' => $this->getThreadId(),
                'status' => $this->getStatusId()
        );
    }

}
