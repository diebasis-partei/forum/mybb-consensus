<?php

require_once(MYBB_ROOT . 'inc/plugins/consensus/dao/DaoBase.php');
require_once(MYBB_ROOT . 'inc/plugins/consensus/models/Status.php');

class StatusDao extends DaoBase
{
    public function __construct(DB_Base $db) {
        parent::__construct($db);
    }

    /**
     * @param $status
     * @return Status
     */
    public function find_status_by_name($status): Status {
        $query = $this->db->simple_select('consensus_status', '*', "status='{$status}'");
        $row = $this->db->fetch_array($query);

        return new Status($row['status'], $row['status_id']);
    }

}