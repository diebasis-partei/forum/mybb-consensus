<?php
require_once(MYBB_ROOT . 'inc/plugins/consensus/dao/DaoBase.php');
require_once(MYBB_ROOT . 'inc/plugins/consensus/models/Proposal.php');

class ProposalDao extends DaoBase
{

    public function __construct(DB_Base $db) {
        parent::__construct($db);
    }

    public function find_proposals(int $consensus_id): array {
        $result = $this->db->simple_select('consensus_proposals', '*', "consensus_id='{$consensus_id}'");

        $proposals = [];
        while((null != ($row = $this->db->fetch_array($result)))) {
            $proposals[] = new Proposal($row['title'], $row['description'], $row['position'], $row['consensus_id'], $row['filename'], $row['proposal_id']);
        }
        return $proposals;
    }

}