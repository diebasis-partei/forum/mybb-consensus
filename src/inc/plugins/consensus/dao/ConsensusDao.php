<?php

require_once(MYBB_ROOT . 'inc/plugins/consensus/models/Proposal.php');
require_once(MYBB_ROOT . 'inc/plugins/consensus/models/Consensus.php');
require_once(MYBB_ROOT . 'inc/plugins/consensus/dao/DaoBase.php');

class ConsensusDao extends DaoBase {

    public function __construct(DB_Base $db) {
        parent::__construct($db);
    }

    public function insert(Consensus $consensus): bool {
        $consensus_array = $this->escape_input($consensus->toDBArray());

        $consensus_id = $this->db->insert_query('consensus', $consensus_array);
        if ($consensus_id > 0) {
            $proposals = $consensus->getProposals();
            foreach ($proposals as $proposal) {
                $proposal->setConsensusId($consensus_id);
                $escaped_proposal = $this->escape_input($proposal->toDBArray());
                $this->db->insert_query('consensus_proposals', $escaped_proposal);
            }
            return true;
        }
        return false;
    }

    public function find_by_id(int $consensus_id): ?Consensus {
        $query = $this->db->simple_select('consensus', '*', "consensus_id='$consensus_id'");
        $db_obj = $this->db->fetch_array($query);

        if (!$db_obj) {
            return null;
        }

        return $this->buildConsensusByDbObject($db_obj);
    }

    public function find_by_thread_id(int $thread_id): ?Consensus {
        $query = $this->db->simple_select('consensus', '*', "thread_id='$thread_id'");
        $db_obj = $this->db->fetch_array($query);

        if (!$db_obj) {
            return null;
        }

        return $this->buildConsensusByDbObject($db_obj);
    }

    public function update_status(int $consensus_id, int $status_id): void {
        $this->db->update_query('consensus', ["status" => $status_id], "consensus_id=$consensus_id");
    }

    public function has_thread_consensus(int $thread_id): bool {
        $query = $this->db->simple_select('consensus', 'COUNT(consensus_id) AS count', "thread_id=$thread_id");
        $count = $this->db->fetch_field($query, 'count');
        return $count > 0;
    }

    /**
     * @param $consensus_id
     * @return array
     */
    private function get_proposals($consensus_id): array
    {
// Get all proposals considering to consensus
        $proposal_query = $this->db->query("SELECT * FROM " . TABLE_PREFIX . "consensus_proposals WHERE consensus_id=$consensus_id ORDER BY position ASC");

        $proposals = array();
        while ($result = $this->db->fetch_array($proposal_query)) {
            $proposals[] = new Proposal($result['title'], $result['description'], $result['position'], $result['consensus_id'], $result['filename'], $result['proposal_id']);
        }
        return $proposals;
    }

    /**
     * @param $db_obj
     * @return Consensus
     */
    private function buildConsensusByDbObject($db_obj): Consensus
    {
        $consensus_id = $db_obj['consensus_id'];
        $title = $db_obj['title'];
        $description = $db_obj['description'];
        $expires = DateTime::createFromFormat("Y-m-d H:i:s", $db_obj['expires']);
        $creator_id = $db_obj['creator'];
        $cthread_id = $db_obj['thread_id'];
        $status_id = $db_obj['status'];
        $proposals = $this->get_proposals($consensus_id);

        return new Consensus($title, $description, $expires, $creator_id, $cthread_id, $status_id, $proposals, $consensus_id);
    }

}