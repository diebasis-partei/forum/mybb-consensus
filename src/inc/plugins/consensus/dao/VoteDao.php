<?php
require_once(MYBB_ROOT . 'inc/plugins/consensus/dao/DaoBase.php');
require_once(MYBB_ROOT . 'inc/plugins/consensus/dao/ProposalDao.php');
require_once(MYBB_ROOT . 'inc/plugins/consensus/models/Vote.php');

class VoteDao extends DaoBase
{
    private ProposalDao $proposal_dao;

    public function __construct(DB_Base $db) {
        parent::__construct($db);
        $this->proposal_dao = new ProposalDao($db);
    }

    public function insert(Vote $vote): bool {
        $escaped_vote = $this->escape_input($vote->toDBArray());
        return $this->db->insert_query('consensus_votes', $escaped_vote) > 0;
    }

    public function update(Vote $vote): void {
        $escaped_vote = $this->escape_input($vote->toDBArray());
        $this->db->update_query('consensus_votes', $escaped_vote, "vote_id={$vote->getVoteId()}");
    }

    public function find_vote(int $user_id, int $proposal_id): ?Vote
    {
        $result = $this->db->simple_select('consensus_votes', '*', "user_id=$user_id AND proposal_id=$proposal_id", array('limit' => '1'));

        $row = $this->db->fetch_array($result);
        if ($row != null) {
            return new Vote($row['proposal_id'], $row['user_id'], $row['points'], $row['vote_id']);
        }

        return null;
    }

    public function find_votes_by_proposal_id(int $proposal_id): array {
        $query = $this->db->simple_select('consensus_votes', '*', "proposal_id=$proposal_id");

        $votes = array();
        while($result = $this->db->fetch_array($query)) {
            $votes[] = new Vote($result['proposal_id'], $result['user_id'], $result['points'], $result['vote_id']);
        }
        return $votes;
    }

    public function has_votes(int $consensus_id, int $user_id): bool {
        $proposals = $this->proposal_dao->find_proposals($consensus_id);

        $proposal_ids = [];
        foreach ($proposals as $proposal) {
            $proposal_ids[] = $proposal->getId();
        }

        $ids = implode(",", $proposal_ids);
        $query = $this->db->simple_select('consensus_votes', '*', "proposal_id IN ($ids) AND user_id=$user_id");
        return $this->db->num_rows($query) > 0;
    }

}