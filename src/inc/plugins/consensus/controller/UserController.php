<?php

class UserController
{
    public static function is_mod($user): bool {
        if ($user != null && $user['ismoderator'] != null) {
            return $user['ismoderator'] == 1;
        }

        return false;
    }


}