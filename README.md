# MyBB - Consensus Plugin

Author: dieBasis - Basisdemokratische Partei Deutschland

Url: https://www.diebasis-partei.de

Version 0.4

License: MIT

## Description

This plugin allows to create, edit and analyse consensus polls.

## Installation

1. Copy the files under the plugin in your MyBB installation.
The list below shows you which file/folder have to be copied to which destination.


| File / Folder         | Destination                               |
|-----------------------|-------------------------------------------|
| consensus.php         | <MYBB_ROOT>/inc/plugins                   |
| new_consensus.php     | <MYBB_ROOT>/                              |
| consensus/consensus/  | <MYBB_ROOT>/inc/plugins                   |
| images/               | <MYBB_ROOT>/images                        |
| language/english      | <MYBB_ROOT>/inc/languages/english         |
| language/german(-sie) | <MYBB_ROOT>/inc/languages/deutsch(_sie)   |

2. After that, you are able to Activate/Install the plugin in the AdminCP under Plugins.

# Upgrade from Version 0.1

Notice: The forum should be set to maintenance mode during upgrade process.

1. Add new column filename in table proposal:
```filename varchar(255)```
   
2. Update templates

**consensus_display_form_consensus**
```
<form method="POST" action="misc.php">
    <input type="hidden" name="action" value="consensus_vote" />
    <input type="hidden" name="consensus_post_code" value="{$mybb->post_code}" />
    <input type="hidden" name="thread_id" value="{$consensus->getThreadId()}" />
    <input type="hidden" name="consensus_id" value="{$consensus->getConsensusId()}">
    <input type="hidden" name="consensus_proposals_size" value="{$number_of_proposals}">
    <table border="0" cellspacing="0" cellpadding="5" class="tborder">
        <thead>
            <tr>
                <td class="thead" colspan="2">
                    <strong>{$lang->consensus}: {$consensus->getTitle()} {$expiry}</strong>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="trow1" style="text-align: left; vertical-align: top;" colspan="2">
                    {$consensus->getDescription()}
                </td>
            </tr>
            <tr>
                <td class="trow1" style="text-align: left; vertical-align: top; font-size: x-small;" colspan="2">
                    <img src="images/icons/information.png" alt="{$lang->consensus_form_tutorial_info}" /> <strong>{$lang->consensus_form_tutorial_title}</strong><br />
                    {$lang->consensus_form_tutorial}
                </td>
            </tr>
            {$proposals}
            <tr>
                <td class="trow1" style="text-align: left; vertical-align: top;">
                    <input type="submit" name="submit" class="button" value="{$lang->consensus_submit}" {$read_mode} /> {$notice_already_voted}
                </td>
                <td class="trow1">
                    {$lang->consensus_resistance_points_scala}
                </td>
            </tr>
        </tbody>
    </table>
</form>
{$close_consensus}
```

**consensus_display_form_proposal**
```
<tr>
    <td class="trow1" style="text-align: left; vertical-align: top;" colspan="2">
        <strong>{$lang->consensus_question} {$proposal->getPosition()} - {$proposal->getTitle()}</strong>
    </td>
</tr>
<tr>
    <td class="trow1" style="text-align: justify; padding-left: 2em;" colspan="2">
        {$proposal->getDescription()}
        <input type="hidden" name="proposal_{$proposal->getPosition()}" value="{$proposal->getId()}">
    </td>
</tr>
{$proposal_image}
<tr>
    <td class="trow1" style="text-align: left;" colspan="2">
        <strong style="margin-right: 2em;">{$lang->consensus_proposal_caption_points}</strong>
        {$points_metric}
    </td>
</tr>
```

**consensus_create_form**
```
<script language="JavaScript">
    function addPoints() {
        let number_of_proposals = document.getElementById("number_points").value;
        
        if (number_of_proposals < 1) {
            number_of_proposals = 1;
        }
        let url = new URL(document.URL);
        url.searchParams.set('proposals', number_of_proposals);
        document.location.href =  url.toString();
    }
</script>
<form method="POST" action="new_consensus.php" enctype="multipart/form-data">
    <input type="hidden" name="consensus_post_code" value="{$mybb->post_code}" />
    <input type="hidden" name="action" value="create" />
    <input type="hidden" name="proposals" value="{$proposals}">
    <input type="hidden" name="tid" value="{$tid}">
    <table border="0" cellspacing="0" cellpadding="5" class="tborder">
        <thead>
            <tr>
                <td class="thead" colspan="2">
                    <strong>{$lang->consensus_add}</strong>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="trow1" style="text-align: justify; padding-right: 1em;" colspan="2">
                    <label for="number_points">{$lang->consensus_proposal_caption_add}:</label><br />
                    <input type="number" id="number_points" value="{$proposals}" min="1" max="10" />
                    <input type="button" onclick="addPoints()" value="{$lang->consensus_proposal_add}" /> {$lang->consensus_proposal_add_notice}
                </td>
            </tr>
            <tr>
                <td class="trow1" style="text-align: left; vertical-align: top; padding-right: 1em;" colspan="2">
                    <label for="consensus_title">{$lang->consensus_title}:</label><br />
                    <input id="consensus_title" name="consensus_title" type="text" maxlength="255" style="width: 100%;" />
                </td>
            </tr>
            <tr>
                <td class="trow1" style="text-align: justify; padding-right: 1em;" colspan="2">
                    <label for="consensus_description">{$lang->consensus_description}:</label><br />
                    <textarea style="width: 100%; height: 5em;"  id="consensus_description" name="consensus_description"></textarea>
                </td>
            </tr>
            <tr>
                <td class="trow1" style="text-align: left;" colspan="2" style="padding-right: 1em;">
                    {$consensus_create_form_proposals}
                </td>
            </tr>
            <tr>
                <td class="trow1" style="text-align; left;">
                    {$lang->consensus_expires}:<br />
                    <input type="datetime-local" name="consensus_expires" class="button" value="{$consensus_default_expiry}" />
                </td>
            </tr>
            <tr>
                <td class="trow1" style="text-align; left;">
                    <input type="submit" name="submit" class="button" value="{$lang->consensus_start}" />
                </td>
            </tr>
        </tbody>
    </table>
</form>
```

**consensus_create_form_proposal**
```
<table id="consensus_point_{$consensus_point_index}" border="0" cellpadding="5" cellspacing="0" class="tborder" style="width: 100%;">
    <tr>
        <td class="trow_selected" style="text-align: left; vertical-align: top; padding-right: 1em;" colspan="2">
            <label for="consensus_proposal_title_{$consensus_point_index}"><strong>{$lang->consensus_proposal_title} {$consensus_point_index}:</strong></label><br />
            <input name="consensus_proposal_title_{$consensus_point_index}" id="consensus_proposal_title_{$consensus_point_index}" type="text" maxlength="255" style="width: 100%;" />
        </td>
    </tr>
    <tr>
        <td class="trow_selected" style="text-align: justify; padding-right: 1em;" colspan="2">
            <label for="consensus_proposal_description_{$consensus_point_index}">{$lang->consensus_proposal_description}:</label><br />
            <textarea style="width: 100%; height: 5em;"  id="consensus_proposal_description_{$consensus_point_index}" name="consensus_proposal_description_{$consensus_point_index}""></textarea>
        </td>
    </tr>
    <tr>
        <td class="trow_selected" style="text-align: justify; padding-right: 1em;" colspan="2">
            <label for="consensus_proposal_image_{$consensus_point_index}">{$lang->consensus_proposal_image}:</label><br />
            <input type="file" id="consensus_proposal_image_{$consensus_point_index}" name="consensus_proposal_image_{$consensus_point_index}" accept="image/*" />
        </td>
    </tr>
</table>
```

**consensus_display_results**
```
<tr>
    <td class="trow1" style="text-align: left; vertical-align: top;" colspan="2">
        <strong>{$lang->consensus_question} {$proposal->getPosition()} - {$proposal->getTitle()}</strong>
    </td>
</tr>
<tr>
    <td class="trow1" style="text-align: justify; padding-left: 2em;" colspan="2">
        {$proposal->getDescription()}
    </td>
</tr>
{$proposal_image}
<tr>
    <td class="trow1" style="text-align: left;" colspan="2">
        <strong style="margin-right: 2em;">{$lang->consensus_results_proposal_results} {$proposal->getPosition()}</strong>
        <table style="text-align: left; width: 100%;">
            {$proposal_results}
        </table>
    </td>
</tr>
<tr>
    <td colspan="2"><hr /></td>
</tr>
```

**consensus_display_results_image (new template)**
```
<tr>
    <td class="trow1" style="text-align: left; vertical-align: top; padding-left: 2em;" colspan="2">
        <div style="">{$lang->consensus_proposal_results_caption_image}:</div>
        <img src="{$proposal_image_path}" style="max-width: 10em; max-height: 10em;" />
    </td>
</tr>
```

3. Replace all files of plugin, delete old class_* files.

That's it.

# Changes

### Version 0.4

Now, it is possible to edit the consensus templates using the mybb template editor without an error. 


## 3rd parties

* [Icons by Aleksandr Reva](https://www.iconfinder.com/Revicon)
